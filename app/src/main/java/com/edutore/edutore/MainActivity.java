package com.edutore.edutore;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private WebView view;
    private String BASEURL = "https://edutore.com/?app=true";
    private SwipeRefreshLayout swipeRefreshLayout;
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        view = (WebView) this.findViewById(R.id.myWeb);
        view.getSettings().setJavaScriptEnabled(true);

        view.getSettings().setAppCacheEnabled(true);

        view.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getPath());
        view.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        view.getSettings().setDomStorageEnabled(true);
        view.getSettings().setUserAgentString(USER_AGENT);

        view.setWebViewClient(new WebViewClient(){
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, String request) {

                if(request.contains("https://edutore01.firebaseapp.com/__/auth/handler?") ){
                    //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(request)));
                    Intent i = new Intent(getApplicationContext(), Auth.class);
                    i.putExtra("URI", Uri.parse(request).toString());
                    startActivity(i);

                    Log.d("RESPON_WEBVIEW", "URL DI KLIK :: "+Uri.parse(request).toString());
                    //if(request.contains("https://accounts.google.com/signin/oauth")){
                    //    view.reload();
                    //}


                }
                return null;

            }
            /*
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String request) {
                if(request.contains("https://edutore01.firebaseapp.com")) {
                   // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(request)));
                    Toast.makeText(getApplicationContext(), Uri.parse(request).toString(), Toast.LENGTH_LONG).show();
                    Log.d("RESPON_WEBVIEW", "URL DI KLIK :: "+Uri.parse(request).toString());
                    return true;
                }
                return true;
            }
            */
            @SuppressWarnings("deprecation")
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                startActivity(new Intent(getApplicationContext(), FailedActivity.class));
                finish();
            }

            @TargetApi(android.os.Build.VERSION_CODES.M)
            @Override
            public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
                onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
                Toast.makeText(MainActivity.this, "Your Internet Connection May not be active Or ",Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), FailedActivity.class));
                finish();
            }



            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                swipeRefreshLayout.setRefreshing(false);
            }
        });
        view.setWebChromeClient(new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if(view.getProgress() == 100){
                    swipeRefreshLayout.setRefreshing(false);
                }else{
                    swipeRefreshLayout.setRefreshing(true);
                }
            }

        });

        view.loadUrl(BASEURL);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.guiRefresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                view.reload();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && view.canGoBack()) {
            view.goBack();
            return true;
        }else{
            new AlertDialog.Builder(this).setMessage("Yakin mau keluar dari Edutore ?")
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    })
                    .setNegativeButton("Batal", null)
                    .show();
        }
        return super.onKeyDown(keyCode, event);
    }
}


