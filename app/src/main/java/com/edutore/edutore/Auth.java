package com.edutore.edutore;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Auth extends AppCompatActivity {

    private WebView view;
    private String BASEURL = "https://edutore.com/";
    private SwipeRefreshLayout swipeRefreshLayout;
    public static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        String newString;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= null;
            } else {
                newString= extras.getString("URI");
            }
        } else {
            newString= (String) savedInstanceState.getSerializable("URI");
        }

        //Toast.makeText(getApplicationContext(), newString, Toast.LENGTH_LONG).show();
        view = (WebView) this.findViewById(R.id.myWebAuth);
        view.getSettings().setJavaScriptEnabled(true);

        view.getSettings().setAppCacheEnabled(true);

        view.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getPath());
        view.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        view.getSettings().setDomStorageEnabled(true);
        view.getSettings().setUserAgentString(USER_AGENT);
        view.setWebViewClient(new WebViewClient());
        view.loadUrl(newString);
    }
}
